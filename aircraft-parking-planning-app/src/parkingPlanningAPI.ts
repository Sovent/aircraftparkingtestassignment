/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

export interface Aircraft {
  registrationCode: string;
  /** @format double */
  footprintSqm: number;
}

export interface Flight {
  aircraft: Aircraft;
  parkingSpot: ParkingSpot;
  /** @format date-time */
  startDateTime: string;
  /** @format date-time */
  endDateTime: string;
}

export interface ParkingArea {
  name: string;
  parkingSpots: ParkingSpot[];
  /** @format double */
  totalSurfaceSqm: number;
}

export interface ParkingSpot {
  name: string;
  /** @format double */
  footprintSqm: number;
}

export interface NewFlight {
  aircraftCode: string;
  parkingSpotId: string
  /** @format date-time */
  startDateTime: string;
  /** @format date-time */
  endDateTime: string;
}

export interface ApiError {
  message: string;
}