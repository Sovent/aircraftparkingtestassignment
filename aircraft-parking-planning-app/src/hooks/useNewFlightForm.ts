import { useMutation, useQuery } from "@tanstack/react-query";
import { useEffect, useState } from "react";
import { addFlight, getAircrafts, getAvailableParkingAreas, getParkingAreas, getTotalAvailableFootprint } from "../api";
import { ApiError } from "../parkingPlanningAPI";

export function useNewFlightForm(initialParkingSpot?: string, startTime?: moment.Moment) {
    const [aircraftName, setAircraftName] = useState('');
    const [parkingStartTime, setParkingStartTime] = useState('');
    const [parkingEndTime, setParkingEndTime] = useState('');
    const [parkingSpot, setParkingSpot] = useState('');
    const { data: availableParkingAreas, isLoading } = useQuery({
        queryKey: ['availableParkingAreas', parkingStartTime, parkingEndTime, aircraftName],
        queryFn: () => {            
            if (parkingStartTime && parkingEndTime && aircraftName) {
                return getAvailableParkingAreas(parkingStartTime, parkingEndTime, aircraftName);
            }

            return getParkingAreas();
        },
    });

    const {data: totalAvailableFootprint} = useQuery({
        queryKey: ['totalAvailableFootprint', parkingStartTime, parkingEndTime],
        queryFn: () => {            
            return getTotalAvailableFootprint(parkingStartTime, parkingEndTime)
        },
        enabled: !!parkingStartTime && !!parkingEndTime
    });

    const { data: aircrafts } = useQuery({
        queryKey: ['aircrafts'],
        queryFn: getAircrafts
    })

    useEffect(() => {
        initialParkingSpot && setParkingSpot(initialParkingSpot)
    }, [initialParkingSpot]);
    useEffect(() => {
        if (startTime) {
            setParkingStartTime(startTime.format("YYYY-MM-DDTHH:MM"));
            setParkingEndTime(startTime.add(1, 'hour').format("YYYY-MM-DDTHH:MM"))
        }
    }, [startTime]);

    const submitMutation = useMutation<void, ApiError>({
        mutationFn: () => addFlight(
            {
                aircraftCode: aircraftName,
                parkingSpotId: parkingSpot,
                startDateTime: parkingStartTime,
                endDateTime: parkingEndTime
            })
    });
    return {
        aircraftName,
        setAircraftName,
        parkingStartTime,
        setParkingStartTime,
        parkingEndTime,
        setParkingEndTime,
        parkingSpot, setParkingSpot,
        availableParkingSpots: availableParkingAreas?.flatMap(area => area.parkingSpots) ?? [],
        aircrafts: aircrafts ?? [],
        totalAvailableFootprint,
        submitMutation
    };
}