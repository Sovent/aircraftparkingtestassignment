import { useQuery, useQueryClient } from "@tanstack/react-query";
import { getFlights, getParkingAreas } from "../api";
import moment, { Moment } from "moment";
import { Id, TimelineGroupBase, TimelineItemBase } from "react-calendar-timeline";
import { useState } from "react";
import { NewFlightFormProps } from "../NewFlightForm";

export function useParkingOverview() {
    const [newFlightFormProps, setNewFlightFormProps] = useState<Omit<NewFlightFormProps, 'onSubmitSuccessful'>>();
    const queryClient = useQueryClient();
    const { data: flights, isError: flightsLoadFailed, isLoading: areFlightsLoading } =
        useQuery({ queryKey: ['getFlights'], queryFn: getFlights });
    const { data: parkingAreas, isError: parkingAreasLoadFailed, isLoading: areParkingAreasLoading } =
        useQuery({ queryKey: ['getParkingAreas'], queryFn: getParkingAreas });
    const groups: TimelineGroupBase[] = parkingAreas?.flatMap(pa => pa.parkingSpots.map(ps => ({ id: ps.name, title: `${ps.name} - ${ps.footprintSqm} sqm` }))) ?? [];
    const items: TimelineItemBase<Moment>[] =
        flights
            ?.filter(f => f.parkingSpot)
            .map(f => ({
                id: `${f.aircraft.registrationCode}-${f.parkingSpot}-${f.startDateTime}`,
                group: f.parkingSpot.name,
                start_time: moment(f.startDateTime),
                end_time: moment(f.endDateTime),
                title: `${f.aircraft.registrationCode} - ${f.aircraft.footprintSqm} sqm`,
            })) ?? [];
    const onTimelineClick = (groupId: Id, time: number) => {
        setNewFlightFormProps({
            parkingSlot: groupId.toString(),
            startTime: moment(time)
        });
    }
    const minDate = moment.min(items.map(i => i.start_time));
    const maxDate = moment.max(items.map(i => i.start_time)).add(1, 'hour');
    return {
        groups, items, onTimelineClick, newFlightFormProps, minDate, maxDate,
        onFormSubmit: () => {
            queryClient.invalidateQueries(["getFlights"]);
            queryClient.invalidateQueries(["getParkingAreas"]);
        }
    };
}