import React, { FormEvent, useState } from 'react';
import "./NewFlightForm.css";
import { useNewFlightForm } from './hooks/useNewFlightForm';

export type NewFlightFormProps = {
    parkingSlot?: string,
    startTime?: moment.Moment,
    onSubmitSuccessful: () => void
}

const NewFlightForm = (props: NewFlightFormProps) => {

    const flightForm = useNewFlightForm(props.parkingSlot, props.startTime);
    const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();

        flightForm.submitMutation.mutate(undefined, { onSuccess: props.onSubmitSuccessful });
    }

    return (
        <div className="container">
            <form onSubmit={handleSubmit} className='form'>
                <div className='row'>
                    <label htmlFor="parking-start-time">Parking Start Time:</label>
                    <input type="datetime-local" id="parking-start-time" value={flightForm.parkingStartTime} onChange={(event) => flightForm.setParkingStartTime(event.target.value)} />
                </div>
                <div className='row'>
                    <label htmlFor="parking-end-time">Parking End Time:</label>
                    <input type="datetime-local" id="parking-end-time" value={flightForm.parkingEndTime} onChange={(event) => flightForm.setParkingEndTime(event.target.value)} />
                </div>

                <div className='row'>
                    <label htmlFor="aircraft-name">Aircraft:</label>
                    <select id="aircraft-name" value={flightForm.aircraftName} onChange={(event) => flightForm.setAircraftName(event.target.value)}>
                        <option value="">-- Please Select --</option>
                        {flightForm.aircrafts.map(aircraft =>
                            <option value={aircraft.registrationCode}>{aircraft.registrationCode} - {aircraft.footprintSqm} sqm</option>
                        )}
                    </select>
                </div>

                <div className='row'>
                    <label htmlFor="parking-slots">Parking spot:</label>
                    <select id="parking-slots" value={flightForm.parkingSpot} onChange={(event) => flightForm.setParkingSpot(event.target.value)}>
                        <option value="">-- Please Select --</option>
                        {flightForm.availableParkingSpots.map(spot =>
                            <option value={spot.name}>{spot.name} - {spot.footprintSqm} sqm</option>
                        )}
                    </select>
                </div>
                {flightForm.submitMutation.isError && 
                    <div className='row'>
                        <p className='error'>Error on attempt to add a flight: {flightForm.submitMutation.error.message ?? "Unknown error"}</p>
                    </div>
                }
                <button type="submit" disabled={flightForm.submitMutation.isLoading}>Submit</button>
            </form>
            <div>
                <div className='row'>
                    {flightForm.totalAvailableFootprint 
                        ? <p>Total available footprint <b>{flightForm.totalAvailableFootprint}</b> sqm</p>
                        : <p>Enter start and end date or double click desired slot in calendar to see the total available footprint</p>
                    }
                </div>

            </div>
        </div>
    );
}

export default NewFlightForm;
