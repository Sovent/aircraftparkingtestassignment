import axios from "axios";
import { Aircraft, Flight, NewFlight, ParkingArea } from "./parkingPlanningAPI";

const instance = axios.create({
    baseURL: 'http://localhost:5070',
    timeout: 1000
});

export async function getFlights() {
    const response = await instance.get<Flight[]>("/flights");
    return response.data;
}

export async function getParkingAreas() {
    const response = await instance.get<ParkingArea[]>("/parkingareas");
    return response.data;
}

export async function addFlight(flight: NewFlight) {
    try {
        await instance.post("/flights", flight);
    }
    catch (err) {
        if (!axios.isAxiosError(err) || err.response?.status !== 400) {
            throw err;
        }
        
        throw err.response.data;
    }    
}

export async function getAircrafts() {
    const response = await instance.get<Aircraft[]>("/aircrafts");
    return response.data;
}

export async function getAvailableParkingAreas(startDate: string, endDate: string, aircraftName: string) {
    const response = await instance.get<ParkingArea[]>(
        "/parkingareas/available",
        {
            params: {
                startTime: startDate,
                endTime: endDate,
                aircraftCode: aircraftName
            }
        });
    return response.data;
}

export async function getTotalAvailableFootprint(startDate: string, endDate: string) {
    const response = await instance.get<number>(
        "/parkingareas/available/totalFootprint",
        {
            params: {
                startTime: startDate,
                endTime: endDate
            }
        });
    return response.data;
}