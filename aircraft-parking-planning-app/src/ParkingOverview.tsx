import Timeline, { } from "react-calendar-timeline";
// make sure you include the timeline stylesheet or the timeline will not be styled
import "react-calendar-timeline/lib/Timeline.css";
import moment from "moment";
import NewFlightForm from "./NewFlightForm";
import { useParkingOverview } from "./hooks/useParkingOverview";

interface P { }

export default function ParkingOverview(props: P) {
  const { groups, items, newFlightFormProps, minDate, maxDate, onTimelineClick, onFormSubmit } = useParkingOverview();
  
  return (
    <div>
      <NewFlightForm {...newFlightFormProps} onSubmitSuccessful={onFormSubmit} />
      <Timeline
        groups={groups}
        items={items}
        canMove={false}
        key={moment().toISOString()}
        onCanvasDoubleClick={onTimelineClick}
        defaultTimeStart={minDate}
        defaultTimeEnd={maxDate} />
    </div>
  );
}
