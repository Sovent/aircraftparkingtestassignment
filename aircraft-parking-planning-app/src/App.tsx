import React from "react";
import ParkingOverview from "./ParkingOverview";
import "./App.css";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";

const queryClient = new QueryClient()

function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <div className="App">
        <ParkingOverview />
      </div>
    </QueryClientProvider>
  );
}

export default App;
