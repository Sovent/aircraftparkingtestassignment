using AircraftParkingPlanning.Domain;
using AircraftParkingPlanning.Model;
using Microsoft.AspNetCore.Mvc;

namespace AircraftParkingPlanning.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ParkingAreasController : ControllerBase
    {
        private readonly ILogger<ParkingAreasController> _logger;
        private readonly IAircraftRepository _aircraftRepository;
        private readonly IParkingAreasRepository _parkingAreasRepository;
        private readonly IFlightsRepository _flightsRepository;
        private readonly IAvailabilityCalculator _availabilityCalculator;

        public ParkingAreasController(
            ILogger<ParkingAreasController> logger,
            IAircraftRepository aircraftRepository,
            IParkingAreasRepository parkingAreasRepository,
            IFlightsRepository flightsRepository,
            IAvailabilityCalculator availabilityCalculator)
        {
            _logger = logger;
            _aircraftRepository = aircraftRepository;
            _parkingAreasRepository = parkingAreasRepository;
            _flightsRepository = flightsRepository;
            _availabilityCalculator = availabilityCalculator;
        }

        [HttpGet(Name = "GetParkingAreas")]
        public IReadOnlyCollection<ParkingArea> Get()
        {
            return _parkingAreasRepository.GetAll();
        }

        [Route("available")]
        [HttpGet]
        [Produces(typeof(IReadOnlyCollection<ParkingArea>))]
        public IActionResult GetAvailable([FromQuery] DateTime startTime, [FromQuery] DateTime endTime,
            [FromQuery] string aircraftCode)
        {
            var aircraft = _aircraftRepository.TryGetByRegistrationCode(aircraftCode);
            if (aircraft == null)
            {
                return NotFound(new Error($"Aircraft with code {aircraftCode} not found"));
            }

            var suitableParkingAreas = _availabilityCalculator.GetAreasWithSuitableParkingAreas(
                _parkingAreasRepository.GetAll(),
                _flightsRepository.GetAll(),
                startTime,
                endTime,
                aircraft.FootprintSqm);
            return Ok(suitableParkingAreas.ToList());
        }

        [Route("available/totalFootprint")]
        [HttpGet]
        public double GetTotalAvailableFootprint([FromQuery] DateTime startTime, [FromQuery] DateTime endTime)
        {
            return _availabilityCalculator.GetTotalVacantFootprint(
                _parkingAreasRepository.GetAll(),
                _flightsRepository.GetAll(),
                startTime,
                endTime);
        }
    }
}