using AircraftParkingPlanning.Domain;
using AircraftParkingPlanning.Model;
using Microsoft.AspNetCore.Mvc;

namespace AircraftParkingPlanning.Controllers
{
  [ApiController]
  [Route("[controller]")]
  public class FlightsController : ControllerBase
  {
    private readonly ILogger<FlightsController> _logger;
    private readonly IFlightsService _flightsService;

    public FlightsController(
      ILogger<FlightsController> logger, 
      IFlightsService flightsService)
    {
      _logger = logger;
      _flightsService = flightsService ?? throw new ArgumentNullException(nameof(flightsService));
    }

    [HttpGet(Name = "GetFlights")]
    public IReadOnlyCollection<Flight> Get()
    {
      return _flightsService.GetAll();
    }

    [HttpPost]
    public IActionResult AddFlight(NewFlight newFlight)
    {
      var addFlightResult = _flightsService.AddFlight(newFlight);
      return addFlightResult.Match<IActionResult>(some => Ok(some), error => BadRequest(error));
    }
  }
}