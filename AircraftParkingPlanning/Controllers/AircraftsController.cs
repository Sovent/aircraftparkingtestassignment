using AircraftParkingPlanning.Domain;
using AircraftParkingPlanning.Model;
using Microsoft.AspNetCore.Mvc;

namespace AircraftParkingPlanning.Controllers;

[ApiController]
[Route("[controller]")]
public class AircraftsController : ControllerBase
{
    private IAircraftRepository _aircraftRepository;

    public AircraftsController(IAircraftRepository aircraftRepository)
    {
        _aircraftRepository = aircraftRepository ?? throw new ArgumentNullException(nameof(aircraftRepository));
    }
    
    [HttpGet(Name = "GetAircrafts")]
    public IReadOnlyCollection<Aircraft> Get()
    {
        return _aircraftRepository.GetAll();
    }
}