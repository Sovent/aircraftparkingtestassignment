using AircraftParkingPlanning;
using AircraftParkingPlanning.Domain;
using AircraftParkingPlanning.Infrastructure;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var setup = new Setup();
builder.Services.AddSingleton<IAircraftRepository>(_ => new InMemoryAircraftRepository(setup.AircraftList));
builder.Services.AddSingleton<IFlightsRepository>(_ => new InMemoryFlightRepository(setup.Flights));
builder.Services.AddSingleton<IParkingAreasRepository>(_ => new InMemoryParkingAreasRepository(setup.ParkingAreas));
builder.Services.AddSingleton<IAvailabilityCalculator, AvailabilityCalculator>();
builder.Services.AddSingleton<IFlightsService, FlightsService>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
  app.UseSwagger();
  app.UseSwaggerUI();
}

app.UseAuthorization();
app.UseCors(options =>
{
  options.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod();
});
app.MapControllers();

app.Run();
