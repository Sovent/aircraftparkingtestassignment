namespace AircraftParkingPlanning.Model;

public record Error(string Message);