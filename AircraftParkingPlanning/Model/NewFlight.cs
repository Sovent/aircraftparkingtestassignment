namespace AircraftParkingPlanning.Model;

public class NewFlight
{
    public string AircraftCode { get; set; }
    public string ParkingSpotId { get; set; }
    public DateTime StartDateTime { get; set; }
    public DateTime EndDateTime { get; set; }
}