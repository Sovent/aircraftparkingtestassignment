﻿namespace AircraftParkingPlanning.Model;

public record ParkingArea(string Name, IReadOnlyCollection<ParkingSpot> ParkingSpots);