using AircraftParkingPlanning.Domain;
using AircraftParkingPlanning.Model;

namespace AircraftParkingPlanning.Infrastructure;

public class InMemoryAircraftRepository : IAircraftRepository
{
    private List<Aircraft> _aircrafts;

    public InMemoryAircraftRepository(List<Aircraft> aircrafts)
    {
        _aircrafts = aircrafts ?? throw new ArgumentNullException(nameof(aircrafts));
    }
    
    public IReadOnlyCollection<Aircraft> GetAll()
    {
        return _aircrafts;
    }

    public Aircraft? TryGetByRegistrationCode(string registrationCode)
    {
        return _aircrafts.SingleOrDefault(aircraft => aircraft.RegistrationCode == registrationCode);
    }
}