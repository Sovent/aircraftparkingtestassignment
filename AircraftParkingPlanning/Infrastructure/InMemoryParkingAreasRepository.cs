using AircraftParkingPlanning.Domain;
using AircraftParkingPlanning.Model;

namespace AircraftParkingPlanning.Infrastructure;

public class InMemoryParkingAreasRepository : IParkingAreasRepository
{
    private readonly List<ParkingArea> _parkingAreas;

    public InMemoryParkingAreasRepository(List<ParkingArea> parkingAreas)
    {
        _parkingAreas = parkingAreas;
    }
    
    public IReadOnlyCollection<ParkingArea> GetAll()
    {
        return _parkingAreas;
    }
}