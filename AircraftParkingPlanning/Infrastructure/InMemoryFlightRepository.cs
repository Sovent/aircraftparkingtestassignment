using System.Collections.Concurrent;
using AircraftParkingPlanning.Domain;
using AircraftParkingPlanning.Model;

namespace AircraftParkingPlanning.Infrastructure;

public class InMemoryFlightRepository : IFlightsRepository
{
    private ConcurrentBag<Flight> _flights;

    public InMemoryFlightRepository(List<Flight> initialFlights)
    {
        if (initialFlights == null) throw new ArgumentNullException(nameof(initialFlights));
        
        _flights = new ConcurrentBag<Flight>(initialFlights);
    }
    
    public IReadOnlyCollection<Flight> GetAll()
    {
        return _flights;
    }

    public void AddFlight(Flight flight)
    {
        _flights.Add(flight);
    }
}