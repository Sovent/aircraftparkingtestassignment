using AircraftParkingPlanning.Model;

namespace AircraftParkingPlanning.Domain;

public interface IAvailabilityCalculator
{
    double GetTotalVacantFootprint(
        IEnumerable<ParkingArea> parkingAreas,
        IEnumerable<Flight> flights,
        DateTime startTime,
        DateTime endTime);

    IEnumerable<ParkingArea> GetAreasWithSuitableParkingAreas(
        IEnumerable<ParkingArea> parkingAreas,
        IEnumerable<Flight> flights,
        DateTime startTime,
        DateTime endTime,
        double requiredFootprint);

    bool IsAvailable(
        ParkingSpot parkingSpot,
        IEnumerable<Flight> flights,
        DateTime startTime,
        DateTime endTime,
        double requiredFootprint);
}