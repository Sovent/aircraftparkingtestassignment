using AircraftParkingPlanning.Model;

namespace AircraftParkingPlanning.Domain;

public interface IFlightsRepository
{
    IReadOnlyCollection<Flight> GetAll();

    void AddFlight(Flight flight);
}