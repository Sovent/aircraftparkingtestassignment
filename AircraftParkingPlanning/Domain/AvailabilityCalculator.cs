using AircraftParkingPlanning.Model;

namespace AircraftParkingPlanning.Domain;

public class AvailabilityCalculator : IAvailabilityCalculator
{
    public IEnumerable<ParkingArea> GetAreasWithSuitableParkingAreas(
        IEnumerable<ParkingArea> parkingAreas,
        IEnumerable<Flight> flights,
        DateTime startTime,
        DateTime endTime,
        double requiredFootprint)
    {
        var parkingSpotsToFlightsLookup = flights.ToLookup(f => f.ParkingSpot.Name);
        return FilterParkingAreas(
            parkingAreas,
            spot => IsVacant(parkingSpotsToFlightsLookup, spot, startTime, endTime)
                    && spot.FootprintSqm >= requiredFootprint);
    }

    public bool IsAvailable(ParkingSpot parkingSpot, IEnumerable<Flight> flights, DateTime startTime, DateTime endTime,
        double requiredFootprint)
    {
        var parkingSpotsToFlightsLookup = flights.ToLookup(f => f.ParkingSpot.Name);
        return IsVacant(parkingSpotsToFlightsLookup, parkingSpot, startTime, endTime)
               && parkingSpot.FootprintSqm >= requiredFootprint;
    }

    public double GetTotalVacantFootprint(
        IEnumerable<ParkingArea> parkingAreas,
        IEnumerable<Flight> flights,
        DateTime startTime,
        DateTime endTime)
    {
        var parkingSpotsToFlightsLookup = flights.ToLookup(f => f.ParkingSpot.Name);
        var vacantAreas = FilterParkingAreas(
            parkingAreas,
            spot => IsVacant(parkingSpotsToFlightsLookup, spot, startTime, endTime));
        var vacantSpots = vacantAreas.SelectMany(area => area.ParkingSpots);
        return vacantSpots.Select(spot => spot.FootprintSqm).DefaultIfEmpty(0).Sum();
    }

    private static IEnumerable<ParkingArea> FilterParkingAreas(
        IEnumerable<ParkingArea> parkingAreas,
        Predicate<ParkingSpot> filter)
    {
        foreach (var area in parkingAreas)
        {
            var matchingParkingSpots = area.ParkingSpots.Where(ps => filter(ps)).ToList();
            if (matchingParkingSpots.Any())
            {
                yield return area with { ParkingSpots = matchingParkingSpots };
            }
        }
    }

    private static bool IsVacant(
        ILookup<string, Flight> parkingSpotsToFlightLookup,
        ParkingSpot parkingSpot,
        DateTime startTime,
        DateTime endTime)
    {
        return !parkingSpotsToFlightLookup[parkingSpot.Name]
            .Any(flight => DoOverlap(flight.StartDateTime, flight.EndDateTime, startTime, endTime));
    }

    private static bool DoOverlap(DateTime startFirst, DateTime endFirst, DateTime startSecond, DateTime endSecond)
    {
        return startFirst <= endSecond && startSecond <= endFirst;
    }
}