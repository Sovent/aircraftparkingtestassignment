using AircraftParkingPlanning.Model;

namespace AircraftParkingPlanning.Domain;

public interface IParkingAreasRepository
{
    IReadOnlyCollection<ParkingArea> GetAll();
}