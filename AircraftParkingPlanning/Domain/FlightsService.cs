using AircraftParkingPlanning.Model;
using OneOf;
using OneOf.Types;
using Error = AircraftParkingPlanning.Model.Error;

namespace AircraftParkingPlanning.Domain;

public class FlightsService : IFlightsService
{
    private readonly IFlightsRepository _flightsRepository;
    private readonly IAircraftRepository _aircraftRepository;
    private readonly IParkingAreasRepository _parkingAreasRepository;
    private readonly IAvailabilityCalculator _availabilityCalculator;

    public FlightsService(IFlightsRepository flightsRepository, IAircraftRepository aircraftRepository,
        IParkingAreasRepository parkingAreasRepository, IAvailabilityCalculator availabilityCalculator)
    {
        _flightsRepository = flightsRepository ?? throw new ArgumentNullException(nameof(flightsRepository));
        _aircraftRepository = aircraftRepository ?? throw new ArgumentNullException(nameof(aircraftRepository));
        _parkingAreasRepository =
            parkingAreasRepository ?? throw new ArgumentNullException(nameof(parkingAreasRepository));
        _availabilityCalculator = availabilityCalculator ?? throw new ArgumentNullException(nameof(availabilityCalculator));
    }

    public IReadOnlyCollection<Flight> GetAll()
    {
        return _flightsRepository.GetAll();
    }

    public OneOf<Some, Error> AddFlight(NewFlight newFlight)
    {
        var aircraft = _aircraftRepository.TryGetByRegistrationCode(newFlight.AircraftCode);
        if (aircraft == null)
        {
            return new Error($"Aircraft with code {newFlight.AircraftCode} not found");
        }

        var readOnlyCollection = _parkingAreasRepository.GetAll();
        var parkingSpot = readOnlyCollection
            .SelectMany(area => area.ParkingSpots)
            .FirstOrDefault(spot => spot.Name == newFlight.ParkingSpotId);
        if (parkingSpot == null)
        {
            return new Error($"Parking spot with id {newFlight.ParkingSpotId} not found");
        }

        var isAvailable = _availabilityCalculator.IsAvailable(
            parkingSpot, 
            _flightsRepository.GetAll(),
            newFlight.StartDateTime, 
            newFlight.EndDateTime, 
            aircraft.FootprintSqm);
        if (!isAvailable)
        {
            return new Error(
                $"Parking spot {parkingSpot.Name} is not available for {aircraft.RegistrationCode} at given time period");
        }
        var flight = new Flight
        {
            Aircraft = aircraft,
            ParkingSpot = parkingSpot,
            StartDateTime = newFlight.StartDateTime,
            EndDateTime = newFlight.EndDateTime
        };
      
        _flightsRepository.AddFlight(flight);
        return new Some();
    }
}