using AircraftParkingPlanning.Model;
using OneOf;
using OneOf.Types;
using Error = AircraftParkingPlanning.Model.Error;

namespace AircraftParkingPlanning.Domain;

public interface IFlightsService
{
    IReadOnlyCollection<Flight> GetAll();
    OneOf<Some, Error> AddFlight(NewFlight newFlight);
}