using AircraftParkingPlanning.Model;

namespace AircraftParkingPlanning.Domain;

public interface IAircraftRepository
{
    IReadOnlyCollection<Aircraft> GetAll();

    Aircraft? TryGetByRegistrationCode(string registrationCode);
}