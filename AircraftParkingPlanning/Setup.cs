﻿using AircraftParkingPlanning.Model;

namespace AircraftParkingPlanning
{
    public class Setup
    {
        public List<Flight> Flights { get; set; }
        public List<Aircraft> AircraftList { get; set; }
        public List<ParkingArea> ParkingAreas { get; set; }

        public Setup()
        {
            CreateAirport();
            CreateAircraftList();
            CreateFlights();
        }

        private void CreateFlights()
        {
            Flights = new List<Flight>();
            Flights.Add(new Flight
            {
                Aircraft = Aircraft("PHNXT"),
                StartDateTime = DateTime.Parse("1 Jan 2023 12:00"),
                EndDateTime = DateTime.Parse("2 Jan 2023 08:00"),
                ParkingSpot = ParkingSpot("N1")
            });
            Flights.Add(new Flight
            {
                Aircraft = Aircraft("9HLTT"),
                StartDateTime = DateTime.Parse("1 Jan 2023 10:00"),
                EndDateTime = DateTime.Parse("3 Jan 2023 12:00"),
                ParkingSpot = ParkingSpot("N2")
            });
            Flights.Add(new Flight
            {
                Aircraft = Aircraft("YUPRJ"),
                StartDateTime = DateTime.Parse("2 Jan 2023 08:30"),
                EndDateTime = DateTime.Parse("3 Jan 2023 12:00"),
                ParkingSpot = ParkingSpot("N1")
            });
            Flights.Add(new Flight
            {
                Aircraft = Aircraft("N123T"),
                StartDateTime = DateTime.Parse("1 Jan 2023 14:30"),
                EndDateTime = DateTime.Parse("1 Jan 2023 20:00"),
                ParkingSpot = ParkingSpot("S1")
            });
            Flights.Add(new Flight
            {
                Aircraft = Aircraft("NCDFT"),
                StartDateTime = DateTime.Parse("1 Jan 2023 09:30"),
                EndDateTime = DateTime.Parse("4 Jan 2023 09:00"),
                ParkingSpot = ParkingSpot("S2")
            });
            Flights.Add(new Flight
            {
                Aircraft = Aircraft("PHNXT"),
                StartDateTime = DateTime.Parse("3 Jan 2023 13:00"),
                EndDateTime = DateTime.Parse("4 Jan 2023 15:00"),
                ParkingSpot = ParkingSpot("N1")
            });
            Flights.Add(new Flight
            {
                Aircraft = Aircraft("ERZ2"),
                StartDateTime = DateTime.Parse("2 Jan 2023 09:30"),
                EndDateTime = DateTime.Parse("3 Jan 2023 09:00"),
                ParkingSpot = ParkingSpot("S3")
            });
        }

        private void CreateAirport()
        {
            ParkingAreas = new List<ParkingArea>
            {
                new("North", new List<ParkingSpot>
                {
                    new() { Name = "N1", FootprintSqm = 700 },
                    new() { Name = "N2", FootprintSqm = 700 },
                    new() { Name = "N3", FootprintSqm = 1400 },
                    new() { Name = "N4", FootprintSqm = 1000 },
                    new() { Name = "N5", FootprintSqm = 1000 },
                    new() { Name = "N6", FootprintSqm = 1500 },
                }),
                new("South", new List<ParkingSpot>
                {
                    new() { Name = "S1", FootprintSqm = 700 },
                    new() { Name = "S2", FootprintSqm = 700 },
                    new() { Name = "S3", FootprintSqm = 1400 },
                    new() { Name = "S4", FootprintSqm = 1000 },
                    new() { Name = "S5", FootprintSqm = 1000 },
                    new() { Name = "S6", FootprintSqm = 1500 },
                    new() { Name = "S7", FootprintSqm = 4500 },
                })
            };
        }

        private ParkingSpot ParkingSpot(string name)
        {
            return ParkingAreas.SelectMany(a => a.ParkingSpots).Where(a => a.Name == name).Single();
        }

        private void CreateAircraftList()
        {
            AircraftList = new List<Aircraft>
            {
                new() { RegistrationCode = "PHNXT", FootprintSqm = 350 },
                new() { RegistrationCode = "9HLTT", FootprintSqm = 600 },
                new() { RegistrationCode = "YUPRJ", FootprintSqm = 420 },
                new() { RegistrationCode = "N123T", FootprintSqm = 550 },
                new() { RegistrationCode = "NCDFT", FootprintSqm = 780 },
                new() { RegistrationCode = "TTPB", FootprintSqm = 490 },
                new() { RegistrationCode = "ZZZZ", FootprintSqm = 1000 },
                new() { RegistrationCode = "ERZ1", FootprintSqm = 1000 },
                new() { RegistrationCode = "ERZ2", FootprintSqm = 3000 },
            };
        }

        private Aircraft Aircraft(string registrationCode)
        {
            return AircraftList.Single(a => a.RegistrationCode == registrationCode);
        }
    }
}