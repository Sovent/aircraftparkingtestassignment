using AircraftParkingPlanning.Domain;
using AircraftParkingPlanning.Model;

namespace AircraftParkingPlannigTests;

public class AvailabilityCalculatorTests
{
    private List<Flight> flights = new()
    {
        new Flight { ParkingSpot = new ParkingSpot { Name = "NA" }, StartDateTime = DateTime.Parse("2023-04-15 10:00:00"), EndDateTime = DateTime.Parse("2023-04-15 11:00:00") },
        new Flight { ParkingSpot = new ParkingSpot { Name = "NC" }, StartDateTime = DateTime.Parse("2023-04-15 12:00:00"), EndDateTime = DateTime.Parse("2023-04-15 14:00:00") },
        new Flight { ParkingSpot = new ParkingSpot { Name = "SD" }, StartDateTime = DateTime.Parse("2023-04-15 16:00:00"), EndDateTime = DateTime.Parse("2023-04-15 17:00:00") }
    };

    private List<ParkingArea> parkingAreas = new()
    {
        new ParkingArea("North", new List<ParkingSpot> { new() { Name = "NA", FootprintSqm = 10 }, new() { Name = "NB", FootprintSqm = 15 }, new() { Name = "NC", FootprintSqm = 20 } }),
        new ParkingArea("South", new List<ParkingSpot> { new() { Name = "SD", FootprintSqm = 10 }, new() { Name = "SE", FootprintSqm = 15 } }),
    };

    private AvailabilityCalculator _availabilityCalculator = new();

    [Fact]
    public void GetSuitableAreas_TakesFlightsFlightsAndFootprintIntoAccount()
    {
        var areas = _availabilityCalculator.GetAreasWithSuitableParkingAreas(
            parkingAreas, 
            flights,
            DateTime.Parse("2023-04-15 13:00:00"), 
            DateTime.Parse("2023-04-15 15:00:00"), 
            12);
        
        Assert.Collection(
            areas, 
            area => Assert.Collection(area.ParkingSpots, spot => Assert.Equal("NB", spot.Name)),
            area => Assert.Collection(area.ParkingSpots, spot => Assert.Equal("SE", spot.Name)));
    }

    [Fact]
    public void GetSuitableAreas_FiltersOutAreasWithoutSuitableParkingSpots()
    {
        var areas = _availabilityCalculator.GetAreasWithSuitableParkingAreas(
            parkingAreas, 
            flights,
            DateTime.Parse("2023-04-15 15:00:00"), 
            DateTime.Parse("2023-04-15 17:00:00"), 
            17);
        
        Assert.Collection(
            areas, 
            area => Assert.Collection(area.ParkingSpots, spot => Assert.Equal("NC", spot.Name)));
    }
    
    [Fact]
    public void GetTotalVacantFootprint_CountsCorrectly()
    {
        var footprint = _availabilityCalculator.GetTotalVacantFootprint(
            parkingAreas, 
            flights,
            DateTime.Parse("2023-04-15 10:30:00"), 
            DateTime.Parse("2023-04-15 13:00:00"));
        // that leaves us with NB, SD, SE
        
        Assert.Equal(40, footprint);
    }
}